import setuptools

setuptools.setup(
    name='agile-sw-dev-course-project',
    version='1.0',
    description='Project to create what-if grade predicter',
    packages=setuptools.find_packages(),
    include_package_data=True,
    #install_requires=['tkinter'],
    entry_points={"console_scripts": ["realpython=UserInterface.runUI"]}
)