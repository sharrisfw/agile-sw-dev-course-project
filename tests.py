import unittest
from GradingSchema import *

class TestCategories(unittest.TestCase):
    
    def setUp(self):
        self.gs = GradingSchema()
    
    def populateGradingSchema(self):
        self.gs.addCategory("Papers",.4)
        self.gs.addCategory("Discussions",.2)
        self.gs.addCategory("Midterm", .2)
        self.gs.addCategory("Final", .2)
        self.gs.addGrade("Papers",77,"actual")
        self.gs.addGrade("Discussions",82,"what-if")
        self.gs.addGrade("Midterm",90,"actual")
        self.gs.setPredictor("Final")
    
    def populateCategories(self):
        self.gs.addCategory("Papers",.4)
        self.gs.addCategory("Discussions",.2)
        self.gs.addCategory("Midterm", .2)
        self.gs.addCategory("Final", .2)

    def test_retrieve_value_from_category(self):
        self.gs.addCategory("Papers")
        self.gs.addCategory("Discussions",.1)
        self.gs.addCategory("Exams", .7, 88, "actual")
        self.assertEqual(self.gs.retrieveValue("Papers"),0)
        self.assertEqual(self.gs.retrieveValue("Discussions"),.1)
        self.assertEqual(self.gs.retrieveValue("Exams"),.7)

    def test_retrieve_all_category_values(self):
        self.gs.addCategory("Papers")
        self.gs.addCategory("Discussions",.1)
        self.assertEqual(self.gs.getAllCategories(), ['Papers','Discussions'])

    def test_add_grade(self):
        self.gs.addCategory("Discussions",.1)
        self.gs.addGrade("Discussions", 90, "actual")
        self.gs.addGrade("Discussions", 80, "what-if")
        self.assertEqual(self.gs.retrieveGrade("Discussions"),80)

    def test_target_score(self):
        self.populateGradingSchema()
        self.gs.addTargetGrade(80)
        self.assertEqual(self.gs.calculateGradeNeeded(),74)

    def test_assignment_grade_calculation(self):
        self.gs.addCategory("Papers",.4)
        self.gs.addAssignment("Papers","Term Paper 1",100,88, 'actual')
        self.gs.addAssignment("Papers","Term Paper 2",100,68, 'actual')
        self.gs.addAssignment("Papers","Term Paper 3",200,90, 'what-if')
        self.gs.addCategory("Exam",.4)
        self.gs.addAssignment("Exam","Exam 1",50,80)
        self.gs.addAssignment("Exam","Exam 2",100)
        self.gs.addAssignmentGrade("Exam","Exam 2",88,'what-if')
        self.gs.addAssignment("Exam","Exam 3",200)
        self.gs.setPredictor("Exam","Exam 3")
        self.gs.addCategory("Discussions",0.2,99)
        self.gs.addTargetGrade(85)
        self.assertEqual(self.gs.calculateGradeNeeded(),74.25)
        self.assertEqual(self.gs.retrieveGrade("Exam","Exam 1"),80)
        self.assertEqual(self.gs.retrieveValue("Exam","Exam 2"),100)
        self.assertEqual(self.gs.retrieveGrade("Discussions"),99)

if __name__ == '__main__':
    unittest.main()