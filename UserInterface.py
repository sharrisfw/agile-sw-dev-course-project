from tkinter import *
from GradingSchema import *

class UserInterface():

    # The function that will pull all of the values from the text fields and plug them into the backend functions in GradingSchema
    def submitCalculation(self):
        gs = GradingSchema()
        for catNum in range(self.catnum):
            if catNum not in self.deletedCategories:
                if self.assignments[catNum] == []:
                    if self.gType[catNum].get()=='pred':
                        gs.addCategory(self.txtName[catNum].get(), float(self.txtValue[catNum].get()))
                        gs.setPredictor(self.txtName[catNum].get())
                    else:
                        gs.addCategory(self.txtName[catNum].get(), float(self.txtValue[catNum].get()), float(self.txtGrade[catNum].get()), self.gType[catNum].get())
                else:
                    gs.addCategory(self.txtName[catNum].get(), float(self.txtValue[catNum].get()))
                    for assignNum in range(len(self.assignments[catNum])):
                        if self.assignments[catNum][assignNum] != {}:
                            if self.assignments[catNum][assignNum]['grade-type'].get()=='pred':
                                gs.addAssignment(self.txtName[catNum].get(), self.assignments[catNum][assignNum]['name'].get(), float(self.assignments[catNum][assignNum]['value'].get()))
                                gs.setPredictor(self.txtName[catNum].get(), self.assignments[catNum][assignNum]['name'].get())
                            else:
                                gs.addAssignment(self.txtName[catNum].get(), self.assignments[catNum][assignNum]['name'].get(), float(self.assignments[catNum][assignNum]['value'].get()), float(self.assignments[catNum][assignNum]['grade'].get()), self.assignments[catNum][assignNum]['grade-type'].get())
        gs.addTargetGrade(float(self.targetGrade.get()))
        self.results.set(gs.calculateGradeNeeded())

    # The function that will clear all text fields
    def clearFields(self):
        for x in range(4):
            self.txtName[x].set('')
            self.txtValue[x].set('')
            self.txtGrade[x].set('')
        self.targetGrade.set('')
        self.results.set('')

    # This function removes a category from the GUI and from consideration by the submitCalculation function
    def removeCategory(self, frame, catNum):
        elements = frame.grid_slaves()
        for element in elements:
            element.destroy()
        frame.destroy()
        self.deletedCategories.append(catNum)

    # This function removes an assignment from the GUI and from consideration by the submitCalculation function
    def deleteAssignment(self, frame, catNum, assignNum):
        elements = frame.grid_slaves()
        for element in elements:
            element.destroy()
        frame.destroy()       
        self.assignments[catNum][assignNum] = {}

    # This function adds an assignment's GUI widgets to the GUI
    def addAssignment(self, frame, frCatNum):
        self.assignments[frCatNum].append({'name':StringVar(),'value':StringVar(),'grade':StringVar(),'grade-type':StringVar()})
        assignNum = len(self.assignments[frCatNum]) - 1

        assignFrame = Frame(frame, bd=2, bg='SkyBlue1')
        assignFrame.grid(row=len(self.assignments[frCatNum])+1, column=1)
        Label(assignFrame, text='Assignment '+ str(assignNum + 1) +':' , bg='SkyBlue1').grid(row=1, column=2)        

        Label(assignFrame, text='Name: ', bg='SkyBlue1').grid(row=2, column=2)
        Entry(assignFrame,textvariable=self.assignments[frCatNum][assignNum]['name']).grid(row=2, column=3)
        Label(assignFrame, text='Value: ', bg='SkyBlue1').grid(row=3, column=2)
        Entry(assignFrame, textvariable=self.assignments[frCatNum][assignNum]['value']).grid(row=3, column=3)
        Radiobutton(assignFrame, text='Predictor', variable= self.assignments[frCatNum][assignNum]['grade-type'], value='pred', justify=LEFT, bg='SkyBlue1').grid(row=3, column=4)
        Radiobutton(assignFrame, text='Actual', variable= self.assignments[frCatNum][assignNum]['grade-type'], value='actual', justify=LEFT, bg='SkyBlue1').grid(row=3, column=5)
        Radiobutton(assignFrame, text='What-if', variable= self.assignments[frCatNum][assignNum]['grade-type'], value='what-if', justify=LEFT, bg='SkyBlue1').grid(row=3, column=6)
        Label(assignFrame, text='Grade: ', bg='SkyBlue1').grid(row=4, column=2)
        Entry(assignFrame, textvariable=self.assignments[frCatNum][assignNum]['grade']).grid(row=4, column=3)
        Button(assignFrame, text='Delete', command= lambda: self.deleteAssignment(assignFrame, frCatNum, assignNum)).grid(row=4, column=4, padx=10)

    # This function adds a category's GUI widgets to the GUI
    def addCategory(self, frame):
        self.txtName.append(StringVar())
        self.txtValue.append(StringVar())
        self.txtGrade.append(StringVar())
        self.gType.append(StringVar())
        self.assignments.append([])

        self.catFrames.append(Frame(frame, bd=10, bg='cornflower blue'))
        self.catFrames[self.catnum].pack(pady=5,padx=2)

        self.catLeftSide.append(Frame(self.catFrames[self.catnum]))
        self.catLeftSide[self.catnum].grid(row=self.catnum + 1, column=1)
        Label(self.catLeftSide[self.catnum], text='Category '+ str(self.catnum + 1) +':', padx=20, bg='cornflower blue', font='Helvetica 14 bold').pack(side=TOP)

        self.EntryFrame.append(Frame(self.catFrames[self.catnum]))
        self.EntryFrame[self.catnum].grid(row=self.catnum + 1, column=2)
        catInfoFrame = Frame(self.EntryFrame[self.catnum])
        catInfoFrame.grid(row=1, column=1)
        Label(catInfoFrame, text='Category Name: ').grid(row=1, column=1)
        Entry(catInfoFrame, textvariable=self.txtName[self.catnum]).grid(row=1, column=2)
        Label(catInfoFrame, text='Value: ').grid(row=2, column=1)
        Entry(catInfoFrame, textvariable=self.txtValue[self.catnum]).grid(row=2, column=2)
        Radiobutton(catInfoFrame, text='Predictor', variable= self.gType[self.catnum], value='pred', justify=LEFT).grid(row=2, column=3)
        Radiobutton(catInfoFrame, text='Actual', variable= self.gType[self.catnum], value='actual', justify=LEFT).grid(row=2, column=4)
        Radiobutton(catInfoFrame, text='What-if', variable= self.gType[self.catnum], value='what-if', justify=LEFT).grid(row=2, column=5)
        Label(catInfoFrame, text='Grade: ').grid(row=3, column=1)
        Entry(catInfoFrame, textvariable=self.txtGrade[self.catnum]).grid(row=3, column=2)

        thisCatNum = self.catnum        
        Button(catInfoFrame, text='Delete Category', command= lambda: self.removeCategory(self.catFrames[thisCatNum], thisCatNum)).grid(row=3, column=4, padx=10)
        Button(catInfoFrame, text='Add New Assignment', command= lambda: self.addAssignment(self.EntryFrame[thisCatNum], thisCatNum)).grid(row=3, column=3, padx=10)
        self.catnum += 1

    # This is the equivalent to the class's main function. It is responsible for instantiating the full GUI including the initial view, button functionality, and class
    # variable instantiation.
    def runUI(self):
        root = Tk()
        root.title('Grade Predictor')
        frame = Frame(root)
        frame.pack()

        self.gType = []
        self.catFrames= []
        self.catLeftSide = []
        self.EntryFrame = []
        self.txtName = []
        self.txtValue = []
        self.txtGrade = []
        self.catnum = 0
        self.deletedCategories = []
        self.assignments = []

        catMainFrame = Frame(root)
        catMainFrame.pack()

        self.addCategory(catMainFrame)

        Button(root, text='Add Category',width=30, command= lambda: self.addCategory(catMainFrame)).pack()

        submitFrame = Frame(root)
        submitFrame.pack()

        Label(submitFrame, text='Target Grade: ').grid(row=1, column=1)
        self.targetGrade = StringVar()
        Entry(submitFrame, textvariable=self.targetGrade).grid(row=1, column=2)

        Label(submitFrame, text='Grade Needed: ').grid(row=2, column=1)
        self.results = StringVar()
        Entry(submitFrame,textvariable=self.results).grid(row=2, column=2)

        Button(submitFrame, text='Submit',width=30, command= lambda: self.submitCalculation()).grid(row=3, column=1)
        Button(submitFrame, text='Clear Values',width=30, command= lambda: self.clearFields()).grid(row=4, column=1)

        root.mainloop()
