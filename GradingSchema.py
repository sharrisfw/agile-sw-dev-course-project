
class GradingSchema():
    def __init__(self):
        self.categories = {}
        self.predictor = []
        self.targetGrade = None

    # Adds category to grading schema with the option to add an associated value, grade, and grade type
    def addCategory(self, newCat, catValue=0, grade=None, grade_type=None):
        self.categories[newCat] = {"value": catValue, "grade" : grade, "type" : grade_type, "assignments" : {}}

    def addAssignment(self, catName, assignName, assignValue=0, assignGrade=None, grade_type=None):
        self.categories[catName]["assignments"][assignName] = {"value" : assignValue, "grade" : assignGrade, "type": grade_type}

    # Returns the value for category based on the name of the category
    def retrieveValue(self, catName, assignName=None):
        if assignName==None or self.categories[catName]["assignments"] == {}:
            return self.categories[catName]["value"]
        else:
            return self.categories[catName]["assignments"][assignName]["value"]

    # Returns the grade for the category based on the name of the category
    def retrieveGrade(self, catName, assignName=None):
        if assignName==None or self.categories[catName]["assignments"] == {}:
            return self.categories[catName]["grade"]
        else:
            return self.categories[catName]["assignments"][assignName]["grade"]
    
    # Returns a list of all the categories in the grading schema
    def getAllCategories(self):
        list = []
        for key in self.categories.keys():
            list.append(key)
        return list

    # Returns a list of all the assignments in a category
    def getAllAssignments(self, catName):
        list = []
        for key in self.categories[catName]["assignments"].keys():
            list.append(key)
        return list

    # Adds a grade to a category or overwrites the grade already there
    def addGrade(self, catName, grade, grade_type):
        self.categories[catName]["grade"] = grade
        self.categories[catName]["type"] = grade_type

    # Adds a grade to an assignment or overwrites the grade already there
    def addAssignmentGrade(self, catName, assignName, grade, grade_type):
        self.categories[catName]["assignments"][assignName]["grade"] = grade
        self.categories[catName]["assignments"][assignName]["type"] = grade_type

    # Sets a category or assignment as the one to be predicted
    def setPredictor(self, catName, assignName=None):
        if assignName==None:
            self.categories[catName]["type"] = 'pred'
        else:
            self.categories[catName]["assignments"][assignName]["type"] = 'pred'
        self.predictor = [catName, assignName]

    # Add overall grade that the user wants to target
    def addTargetGrade(self, target):
        self.targetGrade = target

    # Calculate the grade on the predictor category to get the target grade
    def calculateGradeNeeded(self):
        totalPoints = 0

        for cat in self.getAllCategories():
            if cat != self.predictor[0]:
                if self.categories[cat]["assignments"] == {}:
                    totalPoints += self.categories[cat]["grade"]*self.categories[cat]["value"]
                else:
                    catPoints=0
                    valPoints=0
                    for assign in self.getAllAssignments(cat):
                        catPoints += self.categories[cat]["assignments"][assign]["grade"]*self.categories[cat]["assignments"][assign]["value"]
                        valPoints += self.categories[cat]["assignments"][assign]["value"]
                    totalPoints += catPoints/valPoints * self.categories[cat]["value"]
        
        neededCatGrade = (self.targetGrade - totalPoints)/self.categories[self.predictor[0]]["value"]

        if self.predictor[1] == None:
            neededGrade = neededCatGrade
        else:
            catPoints = 0
            valPoints=0
            for assign in self.getAllAssignments(self.predictor[0]):
                if assign != self.predictor[1]:
                    catPoints += self.categories[self.predictor[0]]["assignments"][assign]["grade"]*self.categories[self.predictor[0]]["assignments"][assign]["value"]
                    valPoints += self.categories[self.predictor[0]]["assignments"][assign]["value"]
            neededGrade = (neededCatGrade*(valPoints+self.categories[self.predictor[0]]["assignments"][self.predictor[1]]["value"]) - catPoints)/self.categories[self.predictor[0]]["assignments"][self.predictor[1]]["value"]

        return round(neededGrade,2)